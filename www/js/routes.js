angular.module('starter').config(config);

function config($stateProvider,
  $urlRouterProvider){
    $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })
    .state('app.index', {
      url: '/index',
      views: {
        'menuContent': {
          templateUrl: 'templates/index.html'
        }
      }
    })
    .state('app.busquedaColegio', {
      url: '/busquedaColegio',
      views: {
        'menuContent': {
          templateUrl: 'templates/busqueda-colegio.html',
          controller: 'BusquedaColegioCtrl'
          }
        }
      })

    ;
    $urlRouterProvider.otherwise('/app/index');
  }
